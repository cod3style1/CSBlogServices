﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CSBlogServices.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bloger",
                columns: table => new
                {
                    BlogerID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bloger", x => x.BlogerID);
                });

            migrationBuilder.CreateTable(
                name: "Room",
                columns: table => new
                {
                    RoomID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Des = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Room", x => x.RoomID);
                });

            migrationBuilder.CreateTable(
                name: "Article",
                columns: table => new
                {
                    ArticleID = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Subtitle = table.Column<string>(nullable: true),
                    CreateAt = table.Column<DateTime>(nullable: false),
                    UpdateAt = table.Column<DateTime>(nullable: false),
                    BlogerID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Article", x => x.ArticleID);
                    table.ForeignKey(
                        name: "FK_Article_Bloger_BlogerID",
                        column: x => x.BlogerID,
                        principalTable: "Bloger",
                        principalColumn: "BlogerID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Profile",
                columns: table => new
                {
                    ProfileID = table.Column<Guid>(nullable: false),
                    CodeName = table.Column<string>(nullable: true),
                    Bio = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    BlogerID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Profile", x => x.ProfileID);
                    table.ForeignKey(
                        name: "FK_Profile_Bloger_BlogerID",
                        column: x => x.BlogerID,
                        principalTable: "Bloger",
                        principalColumn: "BlogerID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Subscribe",
                columns: table => new
                {
                    SubscribeID = table.Column<Guid>(nullable: false),
                    BlogerID = table.Column<Guid>(nullable: false),
                    FollowID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscribe", x => x.SubscribeID);
                    table.ForeignKey(
                        name: "FK_Subscribe_Bloger_BlogerID",
                        column: x => x.BlogerID,
                        principalTable: "Bloger",
                        principalColumn: "BlogerID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Subscribe_Bloger_FollowID",
                        column: x => x.FollowID,
                        principalTable: "Bloger",
                        principalColumn: "BlogerID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    StudentID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Age = table.Column<int>(nullable: false),
                    RoomID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.StudentID);
                    table.ForeignKey(
                        name: "FK_Student_Room_RoomID",
                        column: x => x.RoomID,
                        principalTable: "Room",
                        principalColumn: "RoomID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Delta",
                columns: table => new
                {
                    DeltaID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ArticleID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Delta", x => x.DeltaID);
                    table.ForeignKey(
                        name: "FK_Delta_Article_ArticleID",
                        column: x => x.ArticleID,
                        principalTable: "Article",
                        principalColumn: "ArticleID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PreviewContent",
                columns: table => new
                {
                    PreviewContentID = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    PreviewImage = table.Column<string>(nullable: true),
                    ArticleID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PreviewContent", x => x.PreviewContentID);
                    table.ForeignKey(
                        name: "FK_PreviewContent_Article_ArticleID",
                        column: x => x.ArticleID,
                        principalTable: "Article",
                        principalColumn: "ArticleID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Op",
                columns: table => new
                {
                    OpID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Retain = table.Column<int>(nullable: false),
                    Insert = table.Column<string>(nullable: true),
                    DeltaID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Op", x => x.OpID);
                    table.ForeignKey(
                        name: "FK_Op_Delta_DeltaID",
                        column: x => x.DeltaID,
                        principalTable: "Delta",
                        principalColumn: "DeltaID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tag",
                columns: table => new
                {
                    TagID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    PreviewContentID = table.Column<Guid>(nullable: false),
                    ArticleID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tag", x => x.TagID);
                    table.ForeignKey(
                        name: "FK_Tag_Article_ArticleID",
                        column: x => x.ArticleID,
                        principalTable: "Article",
                        principalColumn: "ArticleID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tag_PreviewContent_PreviewContentID",
                        column: x => x.PreviewContentID,
                        principalTable: "PreviewContent",
                        principalColumn: "PreviewContentID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Attributes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Link = table.Column<string>(nullable: true),
                    List = table.Column<string>(nullable: true),
                    Direction = table.Column<string>(nullable: true),
                    Align = table.Column<string>(nullable: true),
                    Size = table.Column<string>(nullable: true),
                    Script = table.Column<string>(nullable: true),
                    Header = table.Column<int>(nullable: true),
                    Indent = table.Column<int>(nullable: true),
                    Codeblock = table.Column<bool>(nullable: true),
                    Bold = table.Column<bool>(nullable: true),
                    Italic = table.Column<bool>(nullable: true),
                    Underline = table.Column<bool>(nullable: true),
                    Strike = table.Column<bool>(nullable: true),
                    Blockquote = table.Column<bool>(nullable: true),
                    OpID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attributes", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Attributes_Op_OpID",
                        column: x => x.OpID,
                        principalTable: "Op",
                        principalColumn: "OpID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Article_BlogerID",
                table: "Article",
                column: "BlogerID");

            migrationBuilder.CreateIndex(
                name: "IX_Attributes_OpID",
                table: "Attributes",
                column: "OpID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Delta_ArticleID",
                table: "Delta",
                column: "ArticleID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Op_DeltaID",
                table: "Op",
                column: "DeltaID");

            migrationBuilder.CreateIndex(
                name: "IX_PreviewContent_ArticleID",
                table: "PreviewContent",
                column: "ArticleID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Profile_BlogerID",
                table: "Profile",
                column: "BlogerID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Student_RoomID",
                table: "Student",
                column: "RoomID");

            migrationBuilder.CreateIndex(
                name: "IX_Subscribe_BlogerID",
                table: "Subscribe",
                column: "BlogerID");

            migrationBuilder.CreateIndex(
                name: "IX_Subscribe_FollowID",
                table: "Subscribe",
                column: "FollowID");

            migrationBuilder.CreateIndex(
                name: "IX_Tag_ArticleID",
                table: "Tag",
                column: "ArticleID");

            migrationBuilder.CreateIndex(
                name: "IX_Tag_PreviewContentID",
                table: "Tag",
                column: "PreviewContentID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Attributes");

            migrationBuilder.DropTable(
                name: "Profile");

            migrationBuilder.DropTable(
                name: "Student");

            migrationBuilder.DropTable(
                name: "Subscribe");

            migrationBuilder.DropTable(
                name: "Tag");

            migrationBuilder.DropTable(
                name: "Op");

            migrationBuilder.DropTable(
                name: "Room");

            migrationBuilder.DropTable(
                name: "PreviewContent");

            migrationBuilder.DropTable(
                name: "Delta");

            migrationBuilder.DropTable(
                name: "Article");

            migrationBuilder.DropTable(
                name: "Bloger");
        }
    }
}
