﻿using System;
using System.Linq;

namespace CSBlogServices.Interfaces
{
    public interface IRespository<T> where T : class
    {
        IQueryable<T> Query(Func<T, bool> condition);
        IQueryable<T> All { get; }
        IQueryable<T> Select();

        T Find(params object[] keys);

        T Add(T item);
        T Update(T item);
        T Delete(T item);
    }
}
