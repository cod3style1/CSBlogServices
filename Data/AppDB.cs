﻿using System;
using System.Collections.Generic;
using CSBlogModel;
using Microsoft.EntityFrameworkCore;

namespace CSBlogServices.Data
{
    public class AppDB : DbContext
    {
        public AppDB(DbContextOptions<AppDB> options) : base(options)
        {
        }

        public DbSet<Bloger> Bloger { get; set; }
        public DbSet<Profile> Profile { get; set; }
        public DbSet<Subscribe> Subscribe { get; set; }
        public DbSet<Article> Article { get; set; }

        //public DbSet<PreviewContent> PreviewContent { get; set; }
        //public DbSet<Content> Content { get; set; }

        #region test
        public DbSet<Room> Room { get; set; }
        public DbSet<Student> Student { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region ProfileConfig
            modelBuilder.Entity<Profile>()
                .HasKey(p => new { p.ProfileID});

            modelBuilder.Entity<Profile>()
                .HasOne<Bloger>(it => it.Bloger)
                .WithOne(it => it.Profile)
                .HasForeignKey<Profile>(it => it.BlogerID);
            #endregion
            #region Subscribe
            modelBuilder.Entity<Subscribe>(entity =>
            {
                entity.HasKey(it => it.SubscribeID);

                entity.HasOne(it => it.Bloger)
                .WithMany(it => it.Followings)
                .HasForeignKey(it => it.BlogerID)
                .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(it => it.Follow)
                .WithMany(it => it.Followers)
                .HasForeignKey(it => it.FollowID)
                .OnDelete(DeleteBehavior.Restrict);
            });
            #endregion
            #region TagsConfig
            modelBuilder.Entity<Tag>(entity =>
            {
                entity.HasOne(it => it.Article)
                .WithMany(it => it.Tags)
                .HasForeignKey(it => it.ArticleID)
                .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(it => it.PreviewContent)
                .WithMany(it => it.Tags)
                .HasForeignKey(it => it.PreviewContentID)
                .OnDelete(DeleteBehavior.Restrict);
            });
            #endregion
            #region Test
            modelBuilder.Entity<Student>().HasKey(it => it.StudentID);
            modelBuilder.Entity<Room>().HasKey(it => it.RoomID);

            modelBuilder.Entity<Student>()
                 .HasOne(s => s.Room)
                .WithMany(b => b.Students)
                .HasForeignKey(it => it.RoomID);
            #endregion
        }
    }

    public class Room
    {
        public Guid RoomID { get; set; }
        public string Name { get; set; }
        public string Des { get; set; }
        public List<Student> Students { get; set; }
    }

    public class Student
    {
        public Guid StudentID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public Guid RoomID { get; set; }
        public Room Room { get; set; }
    }
}
