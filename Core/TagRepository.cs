﻿using System;
using System.Linq;
using CSBlogModel;
using CSBlogServices.Data;
using Microsoft.EntityFrameworkCore;

namespace CSBlogServices.Core
{
    public class TagRepository : Respository<Tag>
    {
        private readonly AppDB db;

        public TagRepository(AppDB db) : base(db)
        {
            this.db = db;
        }

        public IQueryable<Tag> GetArticleByTag(Guid tagID)
        {
            IQueryable<Tag> tags = db.Set<Tag>().Include(it => it.PreviewContent).Where(it => it.TagID.Equals(tagID)).AsQueryable();
            return tags;
        }
    }
}
