﻿using System;
using CSBlogModel;
using CSBlogServices.Data;

namespace CSBlogServices.Core
{
    public class ArticleRespository : Respository<Article>
    {
        public ArticleRespository(AppDB db) : base(db)
        {
        }
    }
}
