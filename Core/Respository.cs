﻿using System;
using System.Linq;
using CSBlogServices.Data;
using CSBlogServices.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CSBlogServices.Core
{
    public class Respository<T> : IRespository<T> where T : class
    {
        private readonly AppDB db;

        public Respository(AppDB db)
        {
            this.db = db;
        }

        public virtual IQueryable<T> Select() => db.Set<T>().Select(it => it).AsQueryable();
        public IQueryable<T> All => Query(x => true);
        public virtual IQueryable<T> Query(Func<T, bool> condition) => db.Set<T>().Where(condition).AsQueryable();

        public virtual T Find(params object[] keys) => db.Set<T>().Find(keys);

        public virtual T Add(T item)
        {
            var res = db.Set<T>().Add(item).Entity;
            db.SaveChanges();
            return res;
        }

        public virtual T Delete(T item)
        {
            var res = db.Set<T>().Remove(item).Entity;
            db.SaveChanges();
            return res;
        }
        public virtual T Update(T item)
        {
            var res = db.Set<T>().Update(item).Entity;
            db.SaveChanges();
            return res;
        }
    }
}
