﻿using System;
using System.Collections.Generic;
using System.Linq;
using CSBlogModel;
using CSBlogServices.Data;
using Microsoft.EntityFrameworkCore;

namespace CSBlogServices.Core
{
    public class BlogerRespository : Respository<Bloger>
    {
        private readonly AppDB db;

        public BlogerRespository(AppDB db) : base(db) {
            this.db = db;
        }

        public IQueryable<Bloger> GetAll()
        {
            var res = base.Select().Include(it => it.Profile);
            return res;
        }

        public Bloger GetFollow(Guid blogerId)
        {
            Bloger bloger = db.Bloger
                            .Include(it => it.Profile)
                            .Include(it => it.Followers)
                            .Include(it => it.Followings)
                            .FirstOrDefault(it => it.BlogerID.Equals(blogerId));
            //var res = base.Query(it => it.BlogerID.Equals(blogerId)).Include(it => it.Followers).First();
            return bloger;
        }

        public IQueryable<Bloger> GetFollowingById()
        {
            var res = base.Select().Include(it => it.Followings);
            return res;
        }

        public override Bloger Find(params object[] keys)
        {
            return base.Find(keys);
        }

        public Bloger GetByID(Guid blogerId)
        {
            var res = base.Find(blogerId);
            return res;
        }
    }
}
