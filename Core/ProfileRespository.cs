﻿using System;
using CSBlogModel;
using CSBlogServices.Data;

namespace CSBlogServices.Core
{
    public class ProfileRespository : Respository<Profile>
    {
        public ProfileRespository(AppDB db) : base(db)
        {
        }

        public Profile GetByID(Guid profileID)
        {
            return base.Find(profileID);
        }

        public Profile UpdateProfile(ProfileRes item)
        {
            Profile profile =  base.Find(item.ProfileID);
            if (profile == null) throw new Exception("not found profile");
            profile.UpdateProfile(item.CodeName, item.Bio, item.Description);
            return base.Update(profile);
        }
    }
}
