﻿using System;
using CSBlogModel;
using CSBlogServices.Data;

namespace CSBlogServices.Core
{
    public class SubscribeRepository : Respository<Subscribe>
    {
        public SubscribeRepository(AppDB db) : base(db)
        {
        }
    }
}
